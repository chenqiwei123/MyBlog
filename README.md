# My Blog


## 效果预览

### 后台管理页面

- 登录页

	![login](static-files/login.png)

- 后台首页

	![dashboard](static-files/dashboard.png)

- 文章管理

	![blog-list](static-files/blog-list.png)

- 文章编辑

	![edit](static-files/edit.png)

- 评论管理

	![comment-list](static-files/comment-list.png)

- 系统配置

	![config](static-files/config.png)

### 博客展示页面

开发时，在项目中**内置了三套博客主题模板，主题风格各有千秋**，效果如下：

#### 模板一

- 首页

	![index01](static-files/index01.png)

- 文章浏览

	![detail01](static-files/detail01.png)

- 友情链接

	![link01](static-files/link01.png)

#### 模板二

- 首页

	![index02](static-files/index02.png)

- 文章浏览

	![detail02](static-files/detail02.png)

- 友情链接

	![link02](static-files/link02.png)

#### 模板三

- 首页

  ![index03](static-files/index03.png)

- 文章浏览

  ![detail03](static-files/detail03.png)

- 友情链接

  ![link03](static-files/link03.png)
  
## 感谢

- [spring-projects](https://github.com/spring-projects/spring-boot)
- [ColorlibHQ](https://github.com/ColorlibHQ/AdminLTE)
- [tonytomov](https://github.com/tonytomov/jqGrid)
- [pandao](https://github.com/pandao/editor.md)
- [DONGChuan](https://github.com/DONGChuan/Yummy-Jekyll)
- [zjhch123](https://github.com/zjhch123/solo-skin-amaze)
- [t4t5](https://github.com/t4t5/sweetalert)
